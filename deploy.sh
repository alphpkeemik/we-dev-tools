#!/bin/bash
echo -e "\e[00;31mwe deploy script\e[00m"
source config.sh
source _deploy_start.sh

# priv cache update 1/2
PRIVPOOLCACHEFILE=var/cache/we-base-priv-pool.serialized
PRIVPOOLCACHESHA=''
if [  -f $PRIVPOOLCACHEFILE ]; then
	PRIVPOOLCACHESHA="$(sha1sum $PRIVPOOLCACHEFILE)"
fi

rm -rf var/cache/*
composer install
make db-update

make classifier

if [ "$BOWER_ENABLED" = true ] ; then
	php bin/console sp:bower:update
fi
if [ "$MEDIA_ENABLED" = true ] ; then
	php bin/console sonata:classification:fix-context
	php bin/console sonata:media:fix-media-context
fi

if [ "$NOTICE_ENABLED" = true ] ; then
	make noticecontext
fi
if [ "$CMS_ENABLED" = true ] ; then
	php bin/console doctrine:phpcr:repository:init
fi

php bin/console cache:warmup --env=prod --no-debug


make webr

# priv cache update 2/2
php bin/console we:priv:reloadpoolcache
PRIVPOOLCACHESHANEW="$(sha1sum $PRIVPOOLCACHEFILE)"
echo $PRIVPOOLCACHESHANEW;
if [ "$PRIVPOOLCACHESHA" != "$PRIVPOOLCACHESHANEW" ]; then
  echo "  priv cache not same warming up admin priv cache"
  SECONDS=0
  php bin/console we:admin:privcachewarmup
  duration=$SECONDS
  MSG="  duration $(($duration / 60)) minutes and $(($duration % 60)) seconds"
  echo $MSG
fi


if [ $VERSION == 'test' ] ; then	
	CRONCONTENT="$(cat $srcFolder/webroot/crontab)"
	CRONDIR=$srcFolder"webroot";	
	TMPFILE="/tmp/_tmpcroncontent"
	TMPFILE2="/tmp/_tmpcroncontent2"
	echo "" > $TMPFILE	
	crontab -l > $TMPFILE2	
	SEARCH="${CRONDIR//\//\/}"	
    awk "!/$SEARCH/" $TMPFILE2 > $TMPFILE
	
	
	CRONCONTENT="${CRONCONTENT//__PATH__/$CRONDIR}"
	echo "$CRONCONTENT" >> $TMPFILE;

	cat $TMPFILE | crontab -
	rm $TMPFILE
	rm $TMPFILE2
	
fi

# when container from command line is created then log or/and deprecated log was not shown
rm -f var/cache/dev/appDevDebugProject*


source "$DIR/_deploy_end.sh"
